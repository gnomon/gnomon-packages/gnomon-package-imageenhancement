import unittest

import gnomon.core as gnomoncore
import vtk
from gnomon.utils import load_plugin_group

load_plugin_group("imageReader")
load_plugin_group("cellImageFromImage")


class TestGnomonLsmCells(unittest.TestCase):
    """
    Test Gnomon lsm_cells algo class
    """

    def setUp(self):
        self.filenname = "test/resources/qDII-CLV3-PIN1-PI-E35-LD-SAM1-T0-Subset.czi"

        self.reader = gnomoncore.imageReader_pluginFactory().create("imageReaderTimagetk")
        self.reader.setPath(self.filenname)
        self.reader.run()
        self.image = self.reader.image()

        self.segmentation = gnomoncore.cellImageFromImage_pluginFactory().create("lsmCellsSegmentation")

        self.segmented_image = None

    def tearDown(self):
        self.reader.this.disown()
        self.segmentation.this.disown()

    def test_gnomonLsmCells(self):
        self.segmentation.setInput(self.image)
        self.segmentation.refreshParameters()
        self.segmentation.setParameter('membrane_channel', 'ChS1-T2')
        self.segmentation.run()
        self.segmented_image = self.segmentation.output()

        assert self.segmented_image is not None
        image: vtk.vtkImageData = self.image[0].image('ChS1-T2')
        segmented_image = self.segmented_image[0]

        assert image.GetDimensions() == segmented_image.image().GetDimensions()

        assert segmented_image.cellCount() > 1


if __name__ == '__main__':
    unittest.main()
