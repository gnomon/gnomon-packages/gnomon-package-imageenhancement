import unittest

import numpy as np

from gnomon.core import imageReader_pluginFactory, binaryImageReader_pluginFactory, imageFilter_pluginFactory
from gnomon.core import gnomonImage
from gnomon.utils import load_plugin_group

load_plugin_group("imageReader")
load_plugin_group("binaryImageReader")
load_plugin_group("imageFilter")


class TestBinaryImagEnhancement(unittest.TestCase):

    def setUp(self) -> None:
        self.filename = "test/resources/0hrs_plant1_trim-acylYFP_small.inr"

        self.reader =imageReader_pluginFactory().create("imageReaderTimagetk")
        self.reader.setPath(self.filename)
        self.reader.run()
        self.image = self.reader.image()

        self.binary_filename = "test/resources/0hrs_plant1_binary_small.inr.gz"

        self.binary_reader = binaryImageReader_pluginFactory().create("binaryImageReader")
        self.binary_reader.setPath(self.binary_filename)
        self.binary_reader.run()
        self.binary_image = self.binary_reader.binaryImage()

        self.enhancement = imageFilter_pluginFactory().create("edgeEnhancementBinaryimage")

    def test_run(self):
        self.enhancement.setInput(self.image)
        self.enhancement.setMask(self.binary_image)
        self.enhancement.refreshParameters()

        self.enhancement.run()
        enhanced_image = self.enhancement.output()

        assert isinstance(enhanced_image, dict)
        for key, frame in enhanced_image.items():
            assert isinstance(frame, gnomonImage)
            img = frame.data()._image
