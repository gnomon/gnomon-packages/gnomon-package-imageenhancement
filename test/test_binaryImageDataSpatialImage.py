from unittest import TestCase
import numpy as np

import vtk
from gnomon.core import binaryImageData_pluginFactory, gnomonBinaryImage
from gnomon.utils.gnomonPlugin import load_plugin_group
from gnomon_package_imageenhancement.form.binaryImageDataSpatialImage import binaryImageDataSpatialImage
from timagetk import SpatialImage

load_plugin_group("binaryImageData")


class TestbinaryImageDataSpatialImage(TestCase):
    def setUp(self) -> None:
        self.sp_image1 = SpatialImage(np.ones((10, 11, 12), dtype=np.uint8))
        self.form: gnomonBinaryImage = gnomonBinaryImage()
        self.form_data: binaryImageDataSpatialImage = binaryImageData_pluginFactory().create("binaryImageDataSpatialImage")
        self.form.setData(self.form_data)
        self.form_data.set_image(self.sp_image1)

        self.filename = "test/resources/TestGnomonBinaryImageSerialization.txt"
        with open(self.filename, "w") as file:
            file.write(self.form_data.serialize())

    def tearDown(self) -> None:
        self.form.__disown__()
        self.form_data.this.disown()

    def test_image(self):
        vtkimg = self.form.image()
        dims = vtkimg.GetDimensions()
        assert dims == self.sp_image1.shape[::-1]

    # def test_setImage(self):
    #     new_img = SpatialImage(np.random.randint(0, 255, (3, 5, 9), dtype=np.uint8))
    #     dtk_img = sp_img_to_dtk_img(new_img)
    #     print(dtk_img)
    #     self.form.setImage(dtk_img)
    #
    #     assert self.form_data._image.dtype == "bool"
    #     np.testing.assert_equal(self.form_data._image, new_img.astype("bool"))

    def test_serialization(self):
        serialization = self.form.data().serialize()
        with open(self.filename, "r") as file:
            control = file.read()
        assert control == serialization

    def test_deserialization(self):
        with open(self.filename, "r") as file:
            control = file.read()
        new_form_data = binaryImageData_pluginFactory().create("binaryImageDataSpatialImage")
        new_form_data.deserialize(control)
        np.testing.assert_equal(new_form_data._image, self.form.data()._image)
