import unittest

import gnomon.core as gnomoncore
import gnomon.utils.gnomonPlugin
import vtk
from gnomon.utils import load_plugin_group

load_plugin_group("imageReader")
load_plugin_group("imageFilter")


class TestGnomonEdgeIndicator(unittest.TestCase):
    """
    Test Gnomon edge_indicator filter class
    """

    def setUp(self):
        gnomon.utils.gnomonPlugin.DEBUG = True
        self.filename = "test/resources/qDII-CLV3-PIN1-PI-E35-LD-SAM1-T0-Subset.czi"

        self.reader = gnomoncore.imageReader_pluginFactory().create("imageReaderTimagetk")
        self.reader.setPath(self.filename)
        self.reader.run()
        self.image = self.reader.image()

        self.filter = gnomoncore.imageFilter_pluginFactory().create("edgeIndicatorLSM3d")

        self.filtered_image = None

    def tearDown(self):
        self.reader.this.disown()
        self.filter.this.disown()

    def test_gnomonEdgeIndicator(self):
        self.filter.setInput(self.image)
        self.filter.refreshParameters()
        self.filter.setParameter('channel', 'ChS1-T2')
        self.filter.run()
        self.filtered_image = self.filter.output()

        assert self.filtered_image is not None
        image: vtk.vtkImageData = self.image[0].image('ChS1-T2')
        filtered_image: vtk.vtkImageData = self.filtered_image[0].image('ChS1-T2')

        assert image.GetDimensions() == filtered_image.GetDimensions()


if __name__ == '__main__':
    unittest.main()
