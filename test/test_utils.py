from timagetk import SpatialImage
from timagetk.io import imread

from unittest import TestCase
from gnomon_package_imageenhancement.algorithm import utils

class Test(TestCase):
    def test_lsm_contour(self):
        img = imread("test/resources/0hrs_plant1_trim-acylYFP_small.inr")

        out = utils.lsm_contour(img, smooth=1, a=1, b=0, per_down=-0.01, per_up=0.01)
        assert isinstance(out, SpatialImage)
        assert img.shape == out.shape

    def test_lsm_contour_init(self):
        img = imread("test/resources/0hrs_plant1_trim-acylYFP_small.inr")
        img_init = imread("test/resources/0hrs_plant1_binary_small.inr.gz")

        out = utils.lsm_contour_init(img, (img_init != 0).astype("uint8"), smooth=1, a=1, b=0, per_down=-0.01, per_up=0.01)
        assert isinstance(out, SpatialImage)
        assert img.shape == out.shape

    def test_edge_indicator(self):
        img = imread("test/resources/0hrs_plant1_trim-acylYFP_small.inr")

        out = utils.edge_indicator(img, K=0.2, sigma=0)
        assert isinstance(out, SpatialImage)
        assert img.shape == out.shape

        out = utils.edge_indicator(img, K=0.2, sigma=0, filtertype="exp")
        assert isinstance(out, SpatialImage)
        assert img.shape == out.shape
