import unittest

import gnomon.core as gnomoncore
import gnomon.utils.gnomonPlugin
import vtk
from gnomon.utils import load_plugin_group

load_plugin_group("imageReader")
load_plugin_group("binaryImageFromImage")


class TestGnomonBinariazation(unittest.TestCase):
    """
    Test Gnomon binarization plugin
    """

    def setUp(self):
        gnomon.utils.gnomonPlugin.DEBUG = True
        self.filename = "test/resources/qDII-CLV3-PIN1-PI-E35-LD-SAM1-T0-Subset.czi"

        self.reader = gnomoncore.imageReader_pluginFactory().create("imageReaderTimagetk")
        self.reader.setPath(self.filename)
        self.reader.run()
        self.image = self.reader.image()

        self.binarization = gnomoncore.binaryImageFromImage_pluginFactory().create("binarization")

        self.binary_image = None

    def tearDown(self):
        self.reader.this.disown()
        self.binarization.this.disown()

    def test_gnomonEdgeIndicator(self):
        self.binarization.setInput(self.image)
        self.binarization.refreshParameters()
        self.binarization.setParameter('channel', 'ChS1-T2')
        self.binarization.run()
        self.binary_image = self.binarization.output()

        assert self.binary_image is not None
        image: vtk.vtkImageData = self.image[0].image('ChS1-T2')
        binary_image: vtk.vtkImageData = self.binary_image[0].image()

        assert image.GetDimensions() == binary_image.GetDimensions()


if __name__ == '__main__':
    unittest.main()
