import os
import unittest

import gnomon.core as gnomoncore
from gnomon.utils import load_plugin_group

load_plugin_group("binaryImageReader")
load_plugin_group("binaryImageWriter")


class TestGnomonBinaryImageWriter(unittest.TestCase):
    """
    Tests the gnomonBinaryImageWriter class.
    """

    def setUp(self):

        self.filename = "test/resources/0hrs_plant1_binary_small.inr.gz"

        self.reader = gnomoncore.binaryImageReader_pluginFactory().create("binaryImageReader")
        self.reader.setPath(self.filename)

        self.reader.run()
        self.binaryImage = self.reader.binaryImage()

        self.saved_filename = "test/resources/binaryImage_writer_tmp.tif"

        self.writer = gnomoncore.binaryImageWriter_pluginFactory().create("binaryImageWriter")
        self.writer.setPath(self.saved_filename)


    def tearDown(self):
        if os.path.exists(self.saved_filename):
            os.remove(self.saved_filename)
        self.reader.this.disown()
        self.writer.this.disown()


    def test_gnomonBinaryImageWriter_write(self):
        self.writer.setBinaryImage(self.binaryImage)
        self.writer.run()
        assert os.path.exists(self.saved_filename)

        self.reader.setPath(self.saved_filename)
        self.reader.run()
        read_binaryImage = self.reader.binaryImage()
        assert read_binaryImage is not None
        assert read_binaryImage[0].data()._image.sum() == self.binaryImage[0].data()._image.sum()