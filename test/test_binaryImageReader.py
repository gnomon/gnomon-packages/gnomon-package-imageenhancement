from unittest import TestCase

import numpy as np

import gnomon.core as gnomoncore
from gnomon.utils import load_plugin_group

from timagetk import SpatialImage
from timagetk.io import imread

load_plugin_group("binaryImageReader")


class TestBinaryImageReader(TestCase):

    def setUp(self) -> None:
        self.filename = "test/resources/0hrs_plant1_binary_small.inr.gz"

        self.img = imread(self.filename)

    def test_run(self):
        self.reader = gnomoncore.binaryImageReader_pluginFactory().create("binaryImageReader")
        assert self.reader is not None

        # reading file
        self.reader.setPath(self.filename)
        self.reader.run()
        images = self.reader.binaryImage()

        assert isinstance(images, dict)
        for key, frame in images.items():
            assert isinstance(frame, gnomoncore.gnomonBinaryImage)
            img = frame.data()._image
            assert isinstance(img, SpatialImage)
            assert img.dtype == bool
            assert img.size > 0
            assert np.all(img == self.img)


