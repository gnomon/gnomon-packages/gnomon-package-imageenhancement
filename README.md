# Gnomon plugin package : `Image Enhancement`

This package contains Python plugins for the [Gnomon computational platform](https://gnomon.gitlabpages.inria.fr/gnomon/) 
to enhance the signal of 3D intensity images using the tools found in [anifilters](https://gitlab.inria.fr/gcerutti/anifilters)
and [lsm3d](https://gitlab.inria.fr/gcerutti/lsm3d).

It also implements the gnomon form **Binagy Image**.

## Installation

This package is published on the [gnomon anaconda channel](https://anaconda.org/gnomon/gnomon_package_imageenhancement).
To install it a **conda client** is needed such as [miniconda](https://docs.anaconda.com/miniconda/).

To install use the following command in your **gnomon** environment:

```shell
gnomon-utils package install gnomon_package_imageenhancement
```

Alternatively use:
```shell
conda install -c conda-forge -c gnomon -c mosaic -c morpheme -c dtk-forge6 gnomon_package_imageenhancement
```