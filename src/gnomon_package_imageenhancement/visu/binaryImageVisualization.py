import logging
import numpy as np

from matplotlib.colors import _colors_full_map

import vtk
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy, get_vtk_array_type

from visu_core.vtk.utils.matplotlib_tools import vtk_lookuptable_from_mpl_cmap
from visu_core.vtk.utils.polydata_tools import vtk_slice_polydata
from visu_core.matplotlib.colormap import plain_colormap, multicolor_gradient_colormap

from dtkcore import d_inliststring, d_real, d_bool

from gnomon.visualization import gnomonAbstractBinaryImageVtkVisualization

from gnomon.utils import visualizationPlugin
from gnomon.utils.decorators import binaryImageInput

from gnomon_package_imageenhancement.form.binaryImageDataSpatialImage import binaryImageDataSpatialImage

orientation_axes = {0: 'x', 1: 'y', 2: 'z'}
named_colors = [c for c in _colors_full_map.keys() if
                len(c) > 1 and not (("xkcd:" in c) or ("tab:" in c) or ("glasbey" in c))]


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Binary Volume")
@binaryImageInput("image", binaryImageDataSpatialImage)
class binaryImageVisualization(gnomonAbstractBinaryImageVtkVisualization):
    """
    Gnomon binary image visualization
    """

    def __init__(self):
        super().__init__()

        self._parameters = {
            'color': d_inliststring("color", "white", named_colors, "Color to apply to the image"),
            'alpha': d_real("alpha", 1, 0, 1, 2, "Transparency value for the image rendering"),
            'inverse': d_bool("inverse", False, "Inverse the image (true or false visible)")
        }

        self.image = {}
        self.current_image = None
        self.current_time = None

        self.slice_orientation = 2  # to be checked
        self.slice_position = 0

        self.volume_mapper = None
        self.volume_property = None

        self.image_data = None
        self.volume = None
        self.image_colors = None
        self.image_planes = None

        self.renderer3D = None
        self.renderer2D = None
        self.interactor = None

    def __del__(self):
        if self.volume is not None:
            self.volume = None

        if self.image_planes is not None:
            for i, dim in enumerate("xyz"):
                del self.image_planes[i]
            self.image_planes = None

    def pluginName(self):
        return "binaryImageVisualization"

    def update(self):
        if self.current_time is None:
            self.current_time = list(self.image.keys())[0]
        self.current_image = self.image[self.current_time] if not self["inverse"] else np.invert(self.image[self.current_time])
        # print(self.current_image.dtype, self.current_image.max())

        self.renderer3D = self.vtkView().renderer3D()
        self.renderer2D = self.vtkView().renderer2D()

        self.image_data = vtk.vtkImageData()
        image_array = numpy_to_vtk(np.array(self.current_image.astype("uint8")).ravel(),
                                   array_type=get_vtk_array_type("uint8"),
                                   deep=True)
        self.image_data.SetDimensions(self.current_image.shape[::-1])
        self.image_data.SetSpacing(self.current_image.voxelsize[::-1])
        self.image_data.SetOrigin(self.current_image.origin[::-1])
        self.image_data.GetPointData().SetScalars(image_array)

        bounds = self.image_data.GetBounds()
        self.vtkView().setBounds(*bounds)

        if self.volume_mapper is None:
            self.volume_mapper = vtk.vtkSmartVolumeMapper()
        self.volume_mapper.SetInputData(self.image_data)
        self.volume_mapper.SetRequestedRenderModeToGPU()

        lut = vtk_lookuptable_from_mpl_cmap(multicolor_gradient_colormap(['k', self['color']]), value_range=(0, 1))

        alpha = vtk.vtkPiecewiseFunction()
        alpha.RemoveAllPoints()
        alpha.ClampingOn()
        alpha.AddPoint(0, 0)
        opacity = self['alpha']
        alpha.AddPoint(1, opacity)

        if self.volume_property is None:
            self.volume_property = vtk.vtkVolumeProperty()
        self.volume_property.SetColor(lut)
        self.volume_property.SetScalarOpacity(alpha)
        self.volume_property.IndependentComponentsOn()
        self.volume_property.ShadeOn()
        self.volume_property.SetAmbient(0.5)
        self.volume_property.SetDiffuse(0.25)
        self.volume_property.SetSpecular(0.5)
        self.volume_property.SetSpecularPower(3)
        self.volume_property.SetInterpolationTypeToLinear()

        if self.volume is None:
            self.volume = vtk.vtkVolume()
            self.renderer3D.AddActor(self.volume)
        self.volume.SetMapper(self.volume_mapper)
        self.volume.SetProperty(self.volume_property)

        self.vtkView().resetCamera()

        if self.image_colors is None:
            self.image_colors = vtk.vtkImageMapToColors()
            self.image_colors.SetInputData(self.image_data)
        self.image_colors.SetLookupTable(lut)
        self.image_colors.Update()

        if self.image_planes is None:
            self.image_planes = {}
            extent = self.image_data.GetExtent()
            for i, dim in enumerate("xyz"):
                plane = vtk.vtkImageActor()
                plane.SetInputData(self.image_colors.GetOutput())
                if i == 0:
                    pos = (extent[0] + extent[1])//2
                    plane.SetDisplayExtent(pos, pos, extent[2], extent[3], extent[4], extent[5])
                elif i == 1:
                    pos = (extent[2] + extent[3])//2
                    plane.SetDisplayExtent(extent[0], extent[1], pos, pos, extent[4], extent[5])
                elif i == 2:
                    pos = (extent[4] + extent[5])//2
                    plane.SetDisplayExtent(extent[0], extent[1], extent[2], extent[3], pos, pos)
                self.image_planes[i] = plane
                self.renderer2D.AddActor(plane)

        for i, dim in enumerate("xyz"):
            self.image_planes[i].GetProperty().UseLookupTableScalarRangeOn()
            self.image_planes[i].InterpolateOff()
            #self.image_planes[i].GetProperty().SetColor(lut)
            #self.image_planes[i].GetProperty().SetScalarOpacity(alpha)
        self.vtkView().resetCamera()

        self.render()

    def setVisible(self, visible):
        if self.volume is not None:
            self.volume.SetVisibility(visible)
        for i, dim in enumerate("xyz"):
            self.image_planes[i].SetVisibility(visible)

    def imageRendering(self):
        if self.volume is not None:
            self.updateOffscreenRenderer(*self.image_data.GetBounds())
            self.volume_mapper.SetRequestedRenderModeToRayCast()
            self.volume_property.ShadeOff()
            self.volume.Update()
            self.offscreenRenderer().AddActor(self.volume)
        q_image = self.offscreenImageRendering()
        if self.volume is not None:
            self.offscreenRenderer().RemoveActor(self.volume)
            self.volume_mapper.SetRequestedRenderModeToGPU()
            self.volume_property.ShadeOn()
            self.volume.Update()
        return q_image

    def clear(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.volume is not None:
                self.renderer3D.RemoveActor(self.volume)
            if self.image_planes is not None:
                for i, dim in enumerate("xyz"):
                    self.renderer2D.RemoveActor(self.image_planes[i])

    def fill(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.volume is not None:
                self.renderer3D.AddActor(self.volume)
            if self.image_planes is not None:
                for i, dim in enumerate("xyz"):
                    self.renderer2D.AddActor(self.image_planes[i])

    def render(self):
        self.view().render()

    def on2D(self):
        self.render()

    def on3D(self):
        self.render()

    def onXY(self):
        self.render()

    def onXZ(self):
        self.render()

    def onYZ(self):
        self.render()

    def onSliceChanged(self, value):
        self.slice_position = value
        if self.image_planes is not None:
            i = self.slice_orientation
            pos = int(np.round(self.slice_position / self.image_data.GetSpacing()[i]))
            extent = self.image_data.GetExtent()
            if i == 0:
                self.image_planes[i].SetDisplayExtent(pos, pos, extent[2], extent[3], extent[4], extent[5])
            elif i == 1:
                self.image_planes[i].SetDisplayExtent(extent[0], extent[1], pos, pos, extent[4], extent[5])
            elif i == 2:
                self.image_planes[i].SetDisplayExtent(extent[0], extent[1], extent[2], extent[3], pos, pos)
        self.render()

    def onSliceOrientationChanged(self, value):
        self.slice_orientation = value

    def onTimeChanged(self):
        pass
