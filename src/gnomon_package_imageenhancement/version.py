MAJOR = 0
"""(int) Version major component."""

MINOR = 5
"""(int) Version minor component."""

POST = 0
"""(int) Version post or bugfix component."""

__version__ = f"{MAJOR:d}.{MINOR:d}.{POST:d}"
