from dtkcore import d_int, d_inliststring, d_real, d_range_real, array_real_2

import gnomon.core as gnomoncore
from gnomon.core import gnomonAbstractBinaryImageFromImage

from gnomon.utils import load_plugin_group, algorithmPlugin
from gnomon.utils.decorators import imageInput, binaryImageOutput, binaryImageInput

from gnomon_package_tissueimage.form.imageData.gnomonImageDataMultiChannelImage import gnomonImageDataMultiChannelImage
from gnomon_package_imageenhancement.form.binaryImageDataSpatialImage import binaryImageDataSpatialImage

from timagetk import SpatialImage

from . import utils

load_plugin_group("binaryImageData")


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Level-Set Contour")
@imageInput('_image_series', gnomonImageDataMultiChannelImage)
@binaryImageInput('_init_image', binaryImageDataSpatialImage)
@binaryImageOutput('_binary_image', binaryImageDataSpatialImage)
class lsmContour(gnomonAbstractBinaryImageFromImage):
    """Perform an LSM contour of a membrane tissue 3D image.



    """



    def __init__(self):
        super().__init__()

        self._parameters = {}
       # parameters for LSM
        self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel on which to apply the algorithm")
        self._parameters['thresholds'] = d_range_real("Thresholds", array_real_2([5, 20]), 0, 255, "Thresholds for initial contouring")
        self._parameters['area_term'] = d_real("a: Area term", 0.5, 0., 10., 1, "Control the rate of expansion of the objects")
        self._parameters['curvature_term'] = d_real("b: Curvature term", 0., 0., 1., 2, "Control the amount of smoothing constraint onn the objects")
        self._parameters['smooth'] = d_int("Smoothing", 0, 0, 1, "Gaussian blur to apply to the image")
        self._parameters['per_stop'] = d_real("Stoping criterion", 0.002, 0, 0.1, 3, "Stops when absolute growth is less than the criterion for 10 iterations")

        self._image_series = {}
        self._init_image = {}
        self._binary_image = {}

    def refreshParameters(self):
        if len(self._image_series) > 0:
            image = list(self._image_series.values())[0]
            if image is not None:
                if len(image) == 1:
                    if 'channel' in self._parameters.keys():
                        del self._parameters['channel']
                else:
                    if 'channel' not in self._parameters:
                        self._parameters['channel'].setValues([""])
                    channel_name = self['channel']
                    self._parameters['channel'].setValues(list(image.keys()))
                    if channel_name in image.keys():
                        self._parameters['channel'].setValue(channel_name)
                    else:
                        self._parameters['channel'].setValue(list(image.keys())[0])

    def run(self):
        self.set_max_progress(1*len(self._image_series))
        self._binary_image = {}

        for time in self._image_series.keys():
            self.set_progress_message(f"T {time} : finding contour")
            in_image = self._image_series[time]

            if 'channel' in self._parameters.keys():
                img = in_image[self['channel']]
            else:
                img = list(in_image.values())[0]

            if time in self._init_image:
                init_img =  self._init_image[time]

                cont_img = utils.lsm_contour_init(
                    img,
                    init_img.astype("uint8"),
                    a=self['area_term'],
                    b=self['curvature_term'],
                    smooth=self['smooth'],
                    per_down=-self['per_stop'],
                    per_up=self['per_stop'],
                )

            elif len(self._init_image) == 1:
                init_frame, *_ = self._init_image.values()
                if 'channel' in self._parameters.keys() and self['channel'] in init_frame:
                    init_img =  init_frame[self['channel']]
                else:
                    init_img, *_ = init_frame

                cont_img = utils.lsm_contour_init(
                    img,
                    init_img,
                    a=self['area_term'],
                    b=self['curvature_term'],
                    smooth=self['smooth'],
                    per_down=-self['per_stop'],
                    per_up=self['per_stop'],
                )

            else:
                cont_img = utils.lsm_contour(
                    img,
                    lower_threshold=int(self['thresholds'][0]),
                    upper_threshold=int(self['thresholds'][1]),
                    a=self['area_term'],
                    b=self['curvature_term'],
                    smooth=self['smooth'],
                    per_down=-self['per_stop'],
                    per_up=self['per_stop'],
                )
            self.increment_progress()

            self._binary_image[time] = cont_img.astype("bool")
