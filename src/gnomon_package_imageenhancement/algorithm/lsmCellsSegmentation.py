import numpy as np
import scipy.ndimage as nd

from dtkcore import d_int, d_inliststring, d_real

import gnomon.core as gnomoncore

from gnomon.core import gnomonAbstractCellImageFromImage


from gnomon.utils import load_plugin_group, algorithmPlugin
from gnomon.utils.decorators import imageInput, cellImageOutput, pointCloudInput

from gnomon_package_tissueimage.form.imageData.gnomonImageDataMultiChannelImage import gnomonImageDataMultiChannelImage
from gnomon_package_tissueimage.form.cellImageData.gnomonCellImageDataTissueImage import gnomonCellImageDataTissueImage
from gnomon_package_tissueimage.form.pointCloudData.gnomonPointCloudDataPandas import gnomonPointCloudDataPandas

from timagetk import SpatialImage, TissueImage3D
from timagetk.algorithms.morphology import label_filtering
from timagetk.algorithms.watershed import watershed, seeds_detection

from . import utils


_lsm_type_abrv = {
    "image": "i",
    "gradient": "g",
    "hessian": "h",
}

load_plugin_group("cellImageData")


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Level-Set Cells")
@imageInput('_image_series', gnomonImageDataMultiChannelImage)
@pointCloudInput('_seed_points', gnomonPointCloudDataPandas)
@cellImageOutput('_cellimage', gnomonCellImageDataTissueImage)
class lsmCellsSegmentation(gnomonAbstractCellImageFromImage):
    """Perform an LSM segmentation of a membrane tissue 3D image.

    The method performs a segmentation in three steps. First seeds are detected
    for each cell in the tissue using a local intensity minimum detector called
    H-Transform. In a second time, the seeds are eroded or dilated. Finally the
    cells are grown using the LSM algorithm.

    """



    def __init__(self):
        super().__init__()

        self._parameters = {}
        # parameters for seeds detection
        self._parameters['membrane_channel'] = d_inliststring("Membrane channel", "", [""], "Membrane marker channel on which to perform the segmentation")
        self._parameters['h_min'] = d_int("H-min", 2, 0, 255, "High threshold of the H-transform for the detection of seeds")
        self._parameters['gaussian_sigma'] = d_real("Gaussian sigma", 0.5, 0., 50., 2, "Standard deviation of the Gaussian kernel used for the detection of seeds")
        # parameters for LSM
        self._parameters['erosion'] = d_int("Erosion", 0, -2, 2, "Amount of erosion or dilation of seeds for initialisation (dilation if negative)")
        self._parameters['area_term'] = d_real("Area term", 0.5, 0., 10., 1, "Control the rate of expansion of the objects")
        self._parameters['curvature_term'] = d_real("Curvature term", 0., 0., 1., 2, "Control the amount of smoothing constraint onn the objects")
        self._parameters['gamma'] = d_real("Gamma", 1., 0., 50., 1, "Amount of gamma correction on the LSM signal (add contrast in low intensties)")
        self._parameters['smooth'] = d_int("Smoothing", 0, 0, 1, "Gaussian blur to apply to the image")
        self._parameters['lsm_type'] = d_inliststring("LSM type", "gradient", ["image", "gradient", "hessian"], "Boundary type for LSM")
        self._parameters['marge'] = d_int("Marge", 20, 10, 100, "Marge around the seeds for block size")

        self._image_series = {}
        self._seed_points = {}
        self._cellimage = {}

    def refreshParameters(self):
        if len(self._image_series) > 0:
            image = list(self._image_series.values())[0]
            if image is not None:
                if len(image) == 1:
                    if 'membrane_channel' in self._parameters.keys():
                        del self._parameters['membrane_channel']
                else:
                    if 'membrane_channel' not in self._parameters:
                        self._parameters['membrane_channel'].setValues([""])
                    membrane_channel = self['membrane_channel']
                    self._parameters['membrane_channel'].setValues(list(image.keys()))
                    if membrane_channel in image.keys():
                        self._parameters['membrane_channel'].setValue(membrane_channel)
                    else:
                        self._parameters['membrane_channel'].setValue(list(image.keys())[0])

                image = list(image.values())[0]
                if image.dtype == np.uint8:
                    self._parameters['h_min'].setMax(255)
                    self._parameters['h_min'].setValue(2)
                elif image.dtype == np.uint16:
                    self._parameters['h_min'].setMax(65535)
                    self._parameters['h_min'].setValue(200)

    def run(self):
        self.set_max_progress(10*len(self._image_series))
        self._cellimage = {}

        for time in self._image_series.keys():
            in_image = self._image_series[time]

            if 'membrane_channel' in self._parameters.keys():
                img = in_image[self['membrane_channel']]
            else:
                img = list(in_image.values())[0]

            voxelsize = np.array(img.voxelsize)

            self.set_progress_message(f"T {time} : applying gaussian filter")
            smooth_image = nd.gaussian_filter(img, sigma=self['gaussian_sigma'] / voxelsize).astype(img.dtype)
            smooth_img = SpatialImage(smooth_image, voxelsize=voxelsize)
            self.increment_progress()
            self.set_progress_message(f"T {time} : finding seeds")
            seed_img = seeds_detection(smooth_img, h_min=self['h_min'])
            self.increment_progress()
            self.set_progress_message(f"T {time} : computing watershed")
            wat_img = watershed(img, seed_img, labelchoice='first')
            self.increment_progress()
            self.set_progress_message(f"T {time} : applying erosion")
            seed_img = label_filtering(wat_img, method='erosion', radius=1., iterations=2)
            self.increment_progress()

            self.set_progress_message(f"T {time}: refining segmentation with level-set")
            seg_img = utils.lsm_cells(
                img,
                seed_img,
                None,
                erosion=self['erosion'],
                a=self['area_term'],
                b=self['curvature_term'],
                gamma=self['gamma'],
                smooth=self['smooth'],
                lsm_type=_lsm_type_abrv[self['lsm_type']],
                marge=self['marge'],
            )
            self.increment_progress(4)

            self.set_progress_message(f"T {time}: converting output")
            p_img = TissueImage3D(
                seg_img,
                background=0,
                ignore_cells_at_stack_margins=False
            )
            self.increment_progress()

            self.set_progress_message(f"T {time}: computing cells barycenters")
            positions = p_img.cells.barycenter()
            for k, dim in enumerate(['x', 'y', 'z']):
                p_img.cells.set_feature('barycenter_'+dim, {l:p[k] for l, p in positions.items()})
            self.increment_progress()

            self._cellimage[time] = p_img
