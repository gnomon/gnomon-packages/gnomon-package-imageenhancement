import subprocess
from os import path
from tempfile import TemporaryDirectory
from typing import Optional
from glob import glob
import logging

from timagetk.io import imread, imsave
from timagetk.components.spatial_image import SpatialImage
from timagetk.third_party.vt_converter import vt_to_spatial_image
from timagetk.third_party.vt_image import vtImage


def ani3d(image: SpatialImage, K: float = 0.2, sigma: float = 4, gamma: float = 1., upDir: int = 10, nbIter: int = 50)\
        -> SpatialImage:
    """
    Apply an anisotropic filter on image.

    Calls the ani3d program from https://gitlab.inria.fr/gcerutti/anifilters

    Parameters
    ----------
    image : SpatialImage
        input 3d greyscale intensity image in SpatialImage format from timagetk (https://gitlab.inria.fr/mosaic/timagetk)
    K : float, default=0.2
        proportional to the cell walls width [0.1, 0.3]
    sigma : float, default=4
        amount of gaussian blur [2, 8]
    gamma : float, default=1
        correct contrast (<1 : put more white, =1 : no effect, >1 : put more black)
    upDir : int, default=10
        the diffusion tensor is updated every upDiter iteration (>0)
    nbIter : int, default=50
        number of iterations

    Returns
    -------
    filtered_image : SpatialImage
        Output of the anisotropic filter

    """
    with TemporaryDirectory(prefix="ani3d_plugin_") as directory:
        input_image_name = "ani_input.inr"
        imsave(path.join(directory, input_image_name), image)
        completed = subprocess.run(
            ["ani3D", path.join(directory, input_image_name), str(K), str(sigma), str(gamma), str(upDir), str(nbIter)],
            cwd=directory,
            capture_output=True,
            text=True,
            check=True,
        )

        output_loc = None
        for line in completed.stdout.splitlines():
            if line.startswith("image : "):
                logging.debug(line)
                output_loc = line.split()[-1]
                logging.debug("  ->  "+str(output_loc))

        output_image = vt_to_spatial_image(vtImage(str(path.join(directory, output_loc))))
    return output_image


def lsm_cells(
        img: SpatialImage, img_wat: SpatialImage, img_contour: Optional[SpatialImage] = None,
        erosion: int = 0, a: float = 0.5, b: float = 0., gamma: float = 1., smooth: float = 0., lsm_type: str = 'g',
        marge: int = 10
        ):
    """
    Segments img from img_wat as seed using the Level Set Method after a round of erosion/dilatation.

    Calls the lsm_cells program (https://gitlab.inria.fr/gcerutti/lsm3d)
    Parameters
    ----------
    img : SpatialImage
        input 3d greyscale intensity image in SpatialImage format from timagetk (https://gitlab.inria.fr/mosaic/timagetk)
    img_wat : SpatialImage
        image of seeds (cell image)
    img_contour : SpatialImage, default=None
        mask,
        where cells do not evolve. If None cells can evolve everywhere
    erosion : int, default=0
        amount of erosion of seeds for initialisation [-2, 2]
              if 0, then no erosion or dilation
              if negative, then a dilation is performed
    a : {0.5, 0, 1.0}
        area term
            if negative the object retracts
            if positive the object inflates
    b : float, default=0
        curvature term [0, 1]
    gamma : float, default=1
        scale parameter [0.5, 1]
    smooth : int, default=0
        gaussian blur to apply to the image [0, 1]
    lsm_type : {'g', 'i', 'h',}
        gradient, image or hessien based evolution

    Returns
    -------
    segmented_image : SpatialImage
        Segmented image
    """
    with TemporaryDirectory(prefix="lsm3d_plugin_") as directory:
        # saving image input files
        img_path = path.join(directory, "img.inr")
        imsave(img_path, img)

        img_wat_path = path.join(directory, "img_wat.inr")
        imsave(img_wat_path, img_wat)

        if img_contour is None:
            img_contour_path = "None"
        elif isinstance(img_contour, SpatialImage):
            img_contour_path = path.join(directory, "img_contour.inr")
            imsave(img_contour_path, img_contour)

        input_args = [
            "lsm_cells",
            img_path,
            img_wat_path,
            img_contour_path,
            erosion,
            a,
            b,
            gamma,
            smooth,
            lsm_type,
            marge,
        ]
        input_args = [str(x) for x in input_args]
        subprocess.run(
            input_args,
            cwd=directory,
            check=True,
        )

        output_loc = glob(path.join(directory, "img_wat*/*evoEdge.inr.gz"))[0]
        output_image = vt_to_spatial_image(vtImage(str(output_loc)))
    return output_image

def lsm_contour(
        img: SpatialImage, lower_threshold: int = 5, upper_threshold: int = 20,
        a: float = 0., b: float = 0., smooth: int = 1, per_down: float = -0.002, per_up: float = 0.002,
):
    """
    Detects tissue contours in img using the Level Set Method.

    Calls the lsm_contour program (https://gitlab.inria.fr/gcerutti/lsm3d)
    Parameters
    ----------
    img : SpatialImage
        input 3d greyscale intensity image of cells in SpatialImage format from timagetk (https://gitlab.inria.fr/mosaic/timagetk)
    lower_threshold : int, default=5

    upper_threshold : int, default=20
        upper_threshold must be greater than lower_threshold
    a : {0., 0.5, 1.0}
        area term
            if negative the object retracts
            if positive the object inflates
    b : float, default=0
        curvature term [0, 1]
    smooth : int, default=0
        gaussian blur to apply to the image [0, 1]
    per_down : float, default=-0.002
        lower bound for the stopping criterion
    per_up : float, default=0.002
        upper bound for the stopping criterion


    Returns
    -------
    segmented_image : SpatialImage
        Segmented image
    """
    with TemporaryDirectory(prefix="lsm_contour_") as directory:
        # saving image input files
        img_path = path.join(directory, "img.inr")
        imsave(img_path, img)

        input_args = [
            "lsm_contour",
            img_path,
            upper_threshold,
            lower_threshold,
            a,
            b,
            smooth,
            per_up,
            per_down
        ]
        input_args = [str(x) for x in input_args]
        out = subprocess.run(
            input_args,
            cwd=directory,
            check=True,
        )

        output_loc = [path for path in glob(path.join(directory, "img_LSMcont*/*.inr.gz"))
                      if not str(path).endswith("_initial.inr.gz")][0]
        output_image = vt_to_spatial_image(vtImage(str(output_loc)))
    return output_image

def lsm_contour_init(
        img: SpatialImage, initial_contour: SpatialImage,
        a: float = 0., b: float = 0., smooth: int = 1, per_down: float = -0.002, per_up: float = 0.002,
):
    """
    Detects tissue contours in img using the Level Set Method starting from the initial contour defined by initial_contour.

    Calls the lsm_contour program (https://gitlab.inria.fr/gcerutti/lsm3d)
    Parameters
    ----------
    img : SpatialImage
        input 3d greyscale intensity image of cells in SpatialImage format from timagetk (https://gitlab.inria.fr/mosaic/timagetk)
    initial_contour : SpatialImage
         bool image, with values 0=tissue, 1=background
    a : {0., 0.5, 1.0}
        area term
            if negative the object retracts
            if positive the object inflates
    b : float, default=0
        curvature term [0, 1]
    smooth : int, default=0
        gaussian blur to apply to the image [0, 1]
    per_down : float, default=-0.002
        lower bound for the stopping criterion
    per_up : float, default=0.002
        upper bound for the stopping criterion


    Returns
    -------
    segmented_image : SpatialImage
        Segmented image
    """
    with TemporaryDirectory(prefix="lsm_contour_init_") as directory:
        # saving image input files
        img_path = path.join(directory, "img.inr")
        imsave(img_path, img)

        init_contour_path = path.join(directory, "init_contour.inr")
        imsave(init_contour_path, initial_contour)

        input_args = [
            "lsm_contour_init",
            img_path,
            init_contour_path,
            a,
            b,
            smooth,
            per_up,
            per_down
        ]
        input_args = [str(x) for x in input_args]
        out = subprocess.run(
            input_args,
            cwd=directory,
            check=True,
        )

        output_loc = [path for path in glob(path.join(directory, "img_LSMcont*/*.inr.gz"))  if not str(path).endswith("_initial.inr.gz")][0]
        output_image = vt_to_spatial_image(vtImage(str(output_loc)))
    return output_image


def edge_indicator(img: SpatialImage, K: float, sigma: float, filtertype:Optional[str] = None):
    """
    Edge indicator function

    Calls the edge_indicator program (https://gitlab.inria.fr/gcerutti/lsm3d)
    Parameters
    ----------
    img : SpatialImage
        input 3d greyscale intensity image of cells in SpatialImage
        format from timagetk (https://gitlab.inria.fr/mosaic/timagetk)
    K : float
        proportional to width of cell walls (0.1 - 0.3)
    sigma : float
        amount of gaussian blur
    filtertype : str, default=None
        Selects the type of filter. If not specified: 1/(1+(x/K)^2) type edge function.
        If "exp" is marked, then exp(-(x/K)^2) edge function is used


    Returns
    -------
    filterer_image : SpatialImage

    """
    with TemporaryDirectory(prefix="lsm3d_edge_indicator_") as directory:
        # saving image input files
        img_path = path.join(directory, "img.inr")
        imsave(img_path, img)

        input_args = [
            "edge_indicator",
            img_path,
            K,
            sigma,
            filtertype or "",
        ]
        input_args = [str(x) for x in input_args]
        out = subprocess.run(
            input_args,
            cwd=directory,
            check=True,
        )

        output_loc = glob(path.join(directory, "img_edgeind*.inr*"))[0]
        output_image = vt_to_spatial_image(vtImage(str(output_loc)))
    return output_image
