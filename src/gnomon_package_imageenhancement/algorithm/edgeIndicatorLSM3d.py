import numpy as np

from dtkcore import d_inliststring, d_real, d_int

import gnomon.core as gnomoncore
from gnomon.core import gnomonAbstractImageFilter

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput, imageOutput

from timagetk import MultiChannelImage, SpatialImage

from gnomon_package_tissueimage.form.imageData.gnomonImageDataMultiChannelImage import gnomonImageDataMultiChannelImage

from .utils import edge_indicator


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Edge Indicator")
@imageInput('img_dict', gnomonImageDataMultiChannelImage)
@imageOutput('filtered_images', gnomonImageDataMultiChannelImage)
class edgeIndicatorLSM3d(gnomonAbstractImageFilter):
    """ Compute the edge indicator function on an image to find edges.

    The algorithm performs a filtering of the selected channel of a 3D image by
    applying an edge indicator function. Note that, to treat all images the same way
    16 bit images undergo an 8 bit conversion, and some information is lost.

    """
    def __init__(self):
        super().__init__()

        self.img_dict = {}
        self.filtered_images = {}

        self._parameters = {}
        self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel on which to apply the algorithm")
        self._parameters['K'] = d_real("Const K", 0.2, 0.1, 10, 1,"Proportional to the cell walls width")
        self._parameters['sigma'] = d_real("Gaussian Sigma", 0.5, 0., 10., 2, "Standard deviation (in µm) of the Gaussian kernel used to smooth signal")
        self._parameters['filtertype'] = d_inliststring("Filter type", "1/(1+(x/K)^2)", ["1/(1+(x/K)^2)", "exp(-(x/K)^2)"],
                                                        "Type of filter")

    def refreshParameters(self):
        if len(self.img_dict) > 0:
            img_dict = list(self.img_dict.values())[0]
            if len(img_dict) == 1 and 'channel' in self._parameters:
                del self._parameters['channel']
            else:
                if 'channel' not in self._parameters.keys():
                    self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel on which to apply the algorithm")
                self._parameters['channel'].setValues(list(img_dict.keys()))
                self._parameters['channel'].setValue(list(img_dict.keys())[0])

    def run(self):
        self.set_max_progress(1*sum(
            len([
                channel for channel in img.channel_names if 'channels' not in self._parameters.keys() or channel == self['channel']
            ]) for img in self.img_dict.values()
        ))
        self.filtered_images = {}

        for time in self.img_dict.keys():
            in_img = self.img_dict[time]
            self.filtered_images[time] = {}

            for channel in in_img.keys():
                if 'channel' not in self._parameters.keys() or channel == self['channel']:
                    self.set_progress_message(f"T {time} - channel {channel} : computing edge indicator")
                    img = in_img[channel]

                    if img.dtype == np.uint16:
                        img8 = (img/255).astype(np.uint8)
                    elif img.dtype == np.uint8:
                        img8 = img

                    sigma = int(np.round(np.max(self['sigma'] / np.array(img.voxelsize))))

                    enhanced_img = edge_indicator(
                        img8,
                        K=self["K"],
                        sigma=sigma,
                        filtertype="exp" if self["filtertype"] == "exp(-(x/K)^2)" else None,
                    )

                    if img.dtype == np.uint16:
                        self.filtered_images[time][channel] = SpatialImage(enhanced_img.astype(np.uint16)*255)
                    elif img.dtype == np.uint8:
                        self.filtered_images[time][channel] = SpatialImage(enhanced_img)
                    self.increment_progress()
            self.filtered_images[time] = MultiChannelImage(self.filtered_images[time], channel_names=list(self.filtered_images[time].keys()))
