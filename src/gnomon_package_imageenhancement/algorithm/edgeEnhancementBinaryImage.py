import logging

import numpy as np
import scipy.ndimage as nd

from dtkcore import d_int, d_inliststring, d_real, d_bool

import gnomon.core as gnomoncore
from gnomon.core import gnomonAbstractImageFilter

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput, binaryImageInput, imageOutput
from gnomon_package_tissueimage.form.imageData.gnomonImageDataMultiChannelImage import gnomonImageDataMultiChannelImage
from gnomon_package_imageenhancement.form.binaryImageDataSpatialImage import binaryImageDataSpatialImage

from timagetk import SpatialImage, MultiChannelImage


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Mask Edge Enhancement")
@imageInput('img_dict', gnomonImageDataMultiChannelImage)
@binaryImageInput('init', binaryImageDataSpatialImage)
@imageOutput('filtered_images', gnomonImageDataMultiChannelImage)
class edgeEnhancementBinaryimage(gnomonAbstractImageFilter):
    """ Improve image contour by adding binary image edges.

    The algorithm performs edge enhancement by summing the contour from a
    corresponding binary image mask. If no adequate binary image is provided,
    it performs a thresholding of the image intensity and then enhances the
    image contour with it.
    """

    def __init__(self):
        super().__init__()

        self.img_dict = {}
        self.init = {}
        self.filtered_images = {}

        self._parameters = {}
        self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel on which to apply the algorithm")
        self._parameters['threshold'] = d_int("Threshold", 40,0,255, "Intensity threshold used to detect the tissue boundary")
        self._parameters['fill_holes'] = d_bool("Fill holes", False, "Whether to fill the holes in the binary mask")
        self._parameters['boundary_sigma'] = d_real("Boundary sigma", 0.5, 0., 5., 1, "Standard deviation of the Gaussian kernel used to compute the boundary")
        self._parameters['edge_intensity'] = d_int("Edge intensity", 255, 0, 255, "Intensity factor for the added boundary edge")
        self._parameters['gaussian_sigma'] = d_real("Gaussian Sigma", 2., 0., 50., 1, "Standard deviation of the Gaussian kernel used to smooth signal")


    def refreshParameters(self):
        if len(self.img_dict)>0:
            img_dict = list(self.img_dict.values())[0]
            if len(img_dict) == 1:
                if 'channel' in self._parameters:
                    del self._parameters['channel']
            else:
                if 'channel' not in self._parameters.keys():
                    self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel on which to apply the algorithm")
                channel = self['channel']
                self._parameters['channel'].setValues(list(img_dict.keys()))
                if channel in img_dict.keys():
                    self._parameters['channel'].setValue(channel)
                else:
                    self._parameters['channel'].setValue(list(img_dict.keys())[0])

            img = img_dict[list(img_dict.keys())[0]]

            if img.dtype == np.uint8:
                self._parameters['threshold'].setMax(255)
            elif img.dtype == np.uint16:
                self._parameters['threshold'].setMax(65535)
            else:
                self._parameters['threshold'].setMax(img.max())

    def run(self):
        self.set_max_progress(3*sum(
            len([
                channel for channel in img.channel_names if 'channels' not in self._parameters.keys() or channel == self['channel']
            ]) for img in self.img_dict.values()
        ))
        self.filtered_images = {}
        for time in self.img_dict.keys():
            temp_filtered_images = {}

            in_image = self.img_dict[time]

            for channel in in_image.keys():
                if 'channel' not in self._parameters.keys() or channel == self['channel']:

                    self.set_progress_message(f"T {time} - channel {channel} : preparing mask")
                    img = in_image[channel]
                    max_intensity = 65535 if img.dtype==np.uint16 else 255
                    voxelsize = np.array(img.voxelsize)
                    edge_img = {}
                    if len(self.init) > 0:
                        mask = self.init[time]
                    else:
                        mask = (nd.gaussian_filter(img,self['gaussian_sigma']/voxelsize) > self['threshold'])

                    if self['fill_holes']:
                        mask = nd.binary_fill_holes(mask)
                    mask = mask.astype(float)
                    assert mask.shape == img.shape
                    self.increment_progress()

                    self.set_progress_message(f"T {time} - channel {channel} : generating edge image")
                    edge_img = np.linalg.norm([nd.gaussian_filter1d(mask,self['boundary_sigma']/voxelsize[k],order=1,axis=k) for k in range(3)],axis=0)
                    edge_img = self['edge_intensity']*edge_img
                    self.increment_progress()

                    self.set_progress_message(f"T {time} - channel {channel} : enhancing edge in image")
                    enhanced_img = np.minimum(img.get_array().astype(float)+edge_img, max_intensity).astype(img.dtype)
                    enhanced_img = SpatialImage(enhanced_img, voxelsize=img.voxelsize)
                    self.increment_progress()

                    temp_filtered_images[channel] = enhanced_img

                else:
                    logging.debug(f'Channel {channel} is not enhanced')
                    # self.filtered_images[time][channel] = in_image[channel]

                self.filtered_images[time] = MultiChannelImage(list(temp_filtered_images.values()), channel_names=list(temp_filtered_images.keys()))



