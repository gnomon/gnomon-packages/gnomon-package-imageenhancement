import numpy as np

from skimage.filters.thresholding import threshold_otsu
from timagetk import SpatialImage, MultiChannelImage

from dtkcore import d_int, d_inliststring

from gnomon.core import gnomonAbstractBinaryImageFromImage

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput, binaryImageInput, binaryImageOutput


def bin_func(image: MultiChannelImage, threshold: int, channel: str, op: str) -> SpatialImage:
    if channel == "":
        channel = image.get_channel_names()[0]
    if op == 'greater >':
        binary_img = image[channel] > threshold
    else:
        binary_img = image[channel] < threshold
    return binary_img.astype('bool')


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Thresholding")
@imageInput('img_dict', data_plugin="gnomonImageDataMultiChannelImage")
@binaryImageInput('init', data_plugin="binaryImageDataSpatialImage")
@binaryImageOutput('b_img', data_plugin="binaryImageDataSpatialImage")
class binarization(gnomonAbstractBinaryImageFromImage):
    """
    Image Binarization Plugin
    """
    def __init__(self):
        super().__init__()

        self._parameters = {
            'channel': d_inliststring("Channel", "", [""], "Channel on which to apply the algorithm"),
            'greater_or_lower': d_inliststring("> or <", "greater >", ["greater >", "lower <"], "Channel on which to apply the algorithm"),
            "threshold": d_int("Threshold", 127, 0, 255, "Image is true where intensity > threshold"),
        }

        self.connectParameter('channel')

        self.img_dict = {}
        self.init = {}
        self.b_img = {}

    def refreshParameters(self):
        if len(self.img_dict) > 0:
            img_dict = list(self.img_dict.values())[0]
            if len(img_dict) == 1:
                if 'channel' in self._parameters.keys():
                    del self._parameters['channel']
            else:
                if 'channel' not in self._parameters.keys():
                    self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel on which to apply the algorithm")
                    self.connectParameter('channel')
                channel = self['channel']
                self._parameters['channel'].setValues(list(img_dict.keys()))
                if channel in img_dict.keys():
                    self._parameters['channel'].setValue(channel)
                else:
                    self._parameters['channel'].setValue(list(img_dict.keys())[0])

            if len(img_dict) == 1:
                image = list(img_dict.values())[0]
            else:
                image = img_dict[self['channel']]
            if image.dtype == np.uint8:
                self._parameters['threshold'].setMax(255)
            elif image.dtype == np.uint16:
                self._parameters['threshold'].setMax(65535)
            self._set_auto_threshold()

    def onParameterChanged(self, parameter_name):
        if parameter_name == "channel":
            self._set_auto_threshold()

    def _set_auto_threshold(self):
        if len(self.img_dict) > 0:
            img_dict = list(self.img_dict.values())[0]
            if len(img_dict) == 1:
                image = list(img_dict.values())[0]
            else:
                image = img_dict[self['channel']]
            self._parameters['threshold'].setValue(threshold_otsu(image.get_array()))

    def run(self):
        self.set_max_progress(1*len(self.img_dict))
        self.b_img = {}
        for time in self.img_dict.keys():
            self.set_progress_message(f"T {time}: applying threshold")
            in_img = self.img_dict[time]
            if 'channel' in self._parameters:
                self.b_img[time] = bin_func(in_img, self['threshold'], self['channel'], self['greater_or_lower'])
            else:
                self.b_img[time] = bin_func(in_img, self['threshold'], "", self['greater_or_lower'])
            self.increment_progress()
