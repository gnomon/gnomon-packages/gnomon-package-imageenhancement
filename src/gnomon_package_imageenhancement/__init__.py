from . import version

__version__ = version.__version__

# ---- metadata --------------------------------------------------------------------------------------------------------

package = "gnomon-package-imageenhancement"
conda_channel = "gnomon"
