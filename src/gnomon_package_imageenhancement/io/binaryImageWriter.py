import os
import numpy as np

from timagetk.io import imsave

import gnomon.core as gnomoncore
from gnomon.core import gnomonAbstractBinaryImageWriter

from gnomon.utils import algorithmPlugin, seriesWriter
from gnomon.utils.decorators import binaryImageInput

from gnomon_package_imageenhancement.form.binaryImageDataSpatialImage import binaryImageDataSpatialImage


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Binary Image Writer")
@binaryImageInput('b_img', binaryImageDataSpatialImage)
@seriesWriter('b_img', 'filepath')
class binaryImageWriter(gnomonAbstractBinaryImageWriter):
    """
    Binary Image Writer plugin
    """
    def __init__(self):
        super().__init__()

        self.filepath = None
        self.b_img = {}

    def setPath(self, filepath):
        self.filepath = filepath    

    
    def run(self):
        ext = self.filepath.split(".")
        if ext[-1] == "gz":
            ext = ext[-2] + "." +  ext[-1]
        else:
            ext = ext[-1]

        if ext in self.extensions():
            times = np.sort(list(self.b_img.keys()))

            if len(times) > 1:
                for time in times:
                    (dirname, filename) = os.path.split(self.filepath)
                    dirpath = dirname + "/" + os.path.splitext(filename)[0]
                    if not os.path.exists(dirpath):
                        os.makedirs(dirpath)
                    time_filepath = os.path.join(dirpath, os.path.splitext(filename)[0] + "_t" + "%05.2f" % (time) + ext)

                    b_img = self.b_img[time]
                    imsave(time_filepath, b_img.astype("uint8"), force=True)
            else:
                b_img = list(self.b_img.values())[0]
                imsave(self.filepath, b_img.astype("uint8"), force=True)
    
    def extensions(self):
        return ["tif", "inr.gz", "inr"]




