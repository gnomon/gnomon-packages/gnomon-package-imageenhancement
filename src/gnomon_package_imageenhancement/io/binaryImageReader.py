import logging
import os
from pathlib import Path

from timagetk.io import imread

import gnomon.core as gnomoncore
from gnomon.core import gnomonAbstractBinaryImageReader

from gnomon_package_imageenhancement.form.binaryImageDataSpatialImage import binaryImageDataSpatialImage

from gnomon.utils import algorithmPlugin, seriesReader
from gnomon.utils.decorators import binaryImageOutput


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Binary Image Reader")
@binaryImageOutput('binary_images', binaryImageDataSpatialImage)
@seriesReader('binary_images', 'filepath')
class binaryImageReader(gnomonAbstractBinaryImageReader):
    """
    Reader plugin for binaryImages.
    """
    def __init__(self):
        super().__init__()
        self.filepath = ""
        self.binary_images = {}

    def setPath(self, filepath):
        self.filepath = filepath

    def run(self):
        self.binary_images = {}

        if os.path.isdir(self.filepath):
            paths = [os.path.join(self.filepath, path) for path in sorted(os.listdir(self.filepath))]
        else:
            paths = self.filepath.split(",")

        for time, path in enumerate(paths):
            old_path = path
            path = Path(path)
            extension_matching = [path.name.lower().endswith(e) for e in self.extensions()]
            if any(extension_matching) and (path.exists() or old_path.__contains__("http")):
                _ext = self.extensions()[extension_matching.index(True)]
                img_basename = path.name[:-len(_ext)] + _ext
                img_path = path.parent.joinpath(img_basename)
                if old_path.__contains__("http"):
                    img_path = old_path
                img = imread(img_path)
                self.binary_images[time] = img.astype("bool")
            else:
                logging.error("--> Could not find file : "+path)

    def extensions(self):
        return ["inr", "inr.gz", "mha", "tif"]