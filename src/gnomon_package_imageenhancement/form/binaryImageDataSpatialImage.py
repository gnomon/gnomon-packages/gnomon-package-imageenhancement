from copy import deepcopy

import numpy as np
from gnomon_package_tissueimage.utils import vtk_img_to_sp_img, sp_img_to_vtk_img
from timagetk import SpatialImage

import vtk
import gnomon.core as gnomoncore
from gnomon.core import gnomonAbstractBinaryImageData
from gnomon.utils import formDataPlugin, serialize


@formDataPlugin(version="1.0.0", coreversion="1.0.0", data_setter="set_image", data_getter="get_image", name="Binary SpatialImage")
@serialize("_image")
class binaryImageDataSpatialImage(gnomonAbstractBinaryImageData):
    _image: SpatialImage

    def __init__(self):
        super().__init__()
        self._image = None
        self._vtk_image = None

    def __del__(self):
        del self._image

    def set_image(self, image: SpatialImage):
        self._image = image.astype("bool")
        self._vtk_image = None

    def get_image(self) -> SpatialImage:
        return self._image

    def fromGnomonForm(self, form: gnomoncore.gnomonBinaryImage):
        form_data = form.data()
        if isinstance(form_data, binaryImageDataSpatialImage):
            self.set_image(form_data._image)
        else:
            image = form.image()
            self.setImage(image)

    def clone(self):
        clone = binaryImageDataSpatialImage()
        clone.set_image(self._image)
        clone.__disown__()
        return clone

    def metadata(self):
        metadata = {
            "Dimensions": str(self._image.shape),
            "Voxel Size": str(self._image.voxelsize),
        }
        return metadata

    def dataName(self):
        return "timagetk.SpatialImage"

    def image(self) -> vtk.vtkImageData:
        if not self._vtk_image:
            self._vtk_image = sp_img_to_vtk_img(self._image.astype('uint8'))
        return self._vtk_image

    def setImage(self, vtkimage: vtk.vtkImageData):
        self.set_image(dtk_img_to_sp_img(img_copy))
        self._vtk_image = vtkimage
