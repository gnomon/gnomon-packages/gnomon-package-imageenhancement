# ChangeLog

## version 1.0.0 - 2024-03-27
* gnomon 1.0.0
* Move from dtkImage to vtkImageData
* progress bar + message

## version 0.5.0 - 2023-07-07
* libgnomon 0.81.0
* changed default values for parameters of anisotropic3dImageEnhancement
* improved edgeEnhancementBinaryImage with a "fill holes" option
* view buttons
* shared visu

## version 0.4.0 - 2023-04-12
* libgnomon 0.80.0
* parameters connection
* bugfix binarization plugin

## version 0.3.2 - 2023-02-03
* libgnomon 0.72.0
* fix temp image reading
* plugin metadata

## version 0.3.1 - 2022-09-15
* libgnomon 0.71.0
* load images from url
* descriptive image for reader
