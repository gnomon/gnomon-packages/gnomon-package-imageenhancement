#!/usr/bin/env python
#-*- coding: utf-8 -*-

from setuptools import setup, find_packages

short_descr = "Gnomon python plugins to enhance 3D intensity images"
readme = open("README.md")

# find packages
pkgs = find_packages('src')

setup_kwds = dict(
    name='gnomon_package_imageenhancement',
    version="1.0.0",
    description=short_descr,
    long_description=readme.read(),
    author="gnomon-dev",
    author_email="amdt-gnomon-dev@inria.fr",
    url='',
    license='LGPL-3.0-or-later',
    zip_safe=False,

    packages=pkgs,

    package_dir={'': 'src'},
    package_data={
        "": [
            "*.png",
            "*/*.png",
            "*/*/*.png",
            "*.json",
            "*/*.json",
            "*/*/*.json"
        ]
    },

    entry_points={
        'binaryImageData': [
            'binaryImageDataSpatialImage = gnomon_package_imageenhancement.form.binaryImageDataSpatialImage',
        ],
        'binaryImageWriter': [
            'binaryImageWriter = gnomon_package_imageenhancement.io.binaryImageWriter',
        ],
        'binaryImageReader': [
            'binaryImageReader = gnomon_package_imageenhancement.io.binaryImageReader',
        ],
        'binaryImageVtkVisualization': [
            'binaryImageVisualization = gnomon_package_imageenhancement.visu.binaryImageVisualization',
        ],
        'imageFilter': [
            'edgeEnhancementBinaryimage = gnomon_package_imageenhancement.algorithm.edgeEnhancementBinaryImage',
            'edgeIndicatorLSM3d = gnomon_package_imageenhancement.algorithm.edgeIndicatorLSM3d',
            'anisotropic3dImageEnhancement = gnomon_package_imageenhancement.algorithm.anisotropic3dImageEnhancement',
        ],
        'binaryImageFromImage': [
            'lsmContour = gnomon_package_imageenhancement.algorithm.lsmContour',
            'binarization = gnomon_package_imageenhancement.algorithm.binarization',
        ],
        'cellImageFromImage': [
            'lsmCellsSegmentation = gnomon_package_imageenhancement.algorithm.lsmCellsSegmentation',
        ]
    },
    keywords='',

    test_suite='nose.collector',
)
readme.close()
setup(**setup_kwds)
